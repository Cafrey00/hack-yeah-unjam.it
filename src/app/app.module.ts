import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FindTrackComponent} from './panel/find-track/find-track.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './_material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PanelComponent} from './panel/panel.component';
import {StartComponent} from './start/start.component';
import {MapComponent} from './panel/find-track/map/map.component';
import {SidebarComponent} from './panel/find-track/find-track-sidebar/sidebar.component';

@NgModule({
  declarations: [
    AppComponent,
    FindTrackComponent,
    SidebarComponent,
    PanelComponent,
    StartComponent,
    MapComponent
  ],
  imports: [
    MaterialModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
