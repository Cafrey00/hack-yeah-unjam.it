import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FindTrackComponent} from './panel/find-track/find-track.component';
import {PanelComponent} from './panel/panel.component';
import {StartComponent} from './start/start.component';

const routes: Routes = [
  {
    path: '', component: StartComponent
  },
  {
    path: 'panel', component: PanelComponent, children: [
      {path: 'find-track', component: FindTrackComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
