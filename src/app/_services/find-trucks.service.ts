import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {MapPoint} from '../_models/mapPoint.model';
import {map} from 'rxjs/operators';
import {PredefinedVehicle} from '../_models/predefiniedVehicle.model';
import {PathObj} from '../_models/path.model';
import {NodeObj} from '../_models/node.model';

@Injectable({
  providedIn: 'root'
})
export class FindTrucksService {

  apiUrl = 'http://51.68.140.222:8080/TransportManager/';

  constructor(private http: HttpClient) {
  }

  fetchPathComboOptions(): Observable<MapPoint[]> {

    return this.http.get(this.apiUrl + 'destinations').pipe(map((response: any[]) => {
      return response.map((path: any) => {
        return new MapPoint(path.id, path.name);
      });
    }));

  }

  fetchPredefinedVehiclesComboOptions(): Observable<PredefinedVehicle[]> {

    return this.http.get(this.apiUrl + 'predefinedCars').pipe(map((response: any[]) => {
      return response.map((path: any) => {
        return new PredefinedVehicle(path.id, path.name, path.height, path.width, path.mass, path.turningRadius);
      });
    }));

  }

  fetchMapData(): Observable<{ paths: PathObj[], nodes: NodeObj[] }> {

    return this.http.get(this.apiUrl + 'map').pipe(map((response: { nodes: any[], paths: any[] }) => {
      return {
        paths: response.paths.map((path: any) => {
          return new PathObj(path.from, path.maxHeight, path.maxMass, path.maxWidth, path.to);
        }),
        nodes: response.nodes.map((node: any) => {
          return new NodeObj(node.id, node.nodeType, node.x, node.y);
        })
      };
    }));

  }

}
