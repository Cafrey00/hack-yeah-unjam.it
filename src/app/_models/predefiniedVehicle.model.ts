export class PredefinedVehicle {

  id: number;
  name = '';
  height: number;
  width: number;
  mass: number;
  turningRadius: number;

  constructor(id: number, name: string, height: number, width: number, mass: number, turningRadius: number) {

    this.id = id;
    this.name = name;
    this.height = height;
    this.width = width;
    this.mass = mass;
    this.turningRadius = turningRadius;

  }

}
