export class MapPoint {

  id: number;
  name = '';

  constructor(id: number, name: string) {

    this.id = id;
    this.name = name;

  }

}
