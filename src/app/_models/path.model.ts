export class PathObj {

  from: number;
  maxHeight: number;
  maxMass: number;
  maxWidth: number;
  to: number;

  constructor(from: number, maxHeight: number, maxMass: number, maxWidth: number, to: number) {

    this.from = from;
    this.maxHeight = maxHeight;
    this.maxMass = maxMass;
    this.maxWidth = maxWidth;
    this.to = to;

  }

}
