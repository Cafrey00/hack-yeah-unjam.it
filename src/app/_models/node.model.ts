export class NodeObj {

  id: number;
  nodeType = '';
  x: number;
  y: number;

  constructor(id: number, nodeType: string, x: number, y: number) {

    this.id = id;
    this.nodeType = nodeType;
    this.x = x;
    this.y = y;

  }

}
