import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule,
  MatMenuModule,
  MatSelectModule,
  MatSidenavModule, MatStepperModule, MatVerticalStepper
} from '@angular/material';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatMenuModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    MatInputModule,
    MatIconModule,
    MatStepperModule,
  ],
  exports: [
    MatSidenavModule,
    MatCheckboxModule,
    MatMenuModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    MatInputModule,
    MatIconModule,
    MatStepperModule,
  ]
})
export class MaterialModule {
}
