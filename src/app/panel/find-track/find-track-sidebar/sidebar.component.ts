import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FindTrucksService} from '../../../_services/find-trucks.service';
import {PredefinedVehicle} from '../../../_models/predefiniedVehicle.model';
import {MapPoint} from '../../../_models/mapPoint.model';

@Component({
  selector: 'app-find-track-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {

  form: FormGroup = null;

  paths: MapPoint[] = [];
  predefinedVehicles: PredefinedVehicle[] = [];

  constructor(private formBuilder: FormBuilder,
              private findTrackService: FindTrucksService) {
  }

  ngOnInit() {

    this.buildForm();

    this.fetchPathComboOptions();
    this.fetchPredefinedVehiclesComboOptions();

    this.registerEvents();

  }

  registerEvents() {

    this.form.get('choosenVehicle').valueChanges.subscribe((vehicle: PredefinedVehicle) => {
      this.setVehicle(vehicle);
    });

    this.form.get('vehicleEdited').valueChanges.subscribe((editMode: boolean) => {
        if (editMode) {
          this.enableEditVehicleInputs();
        } else {
          const choosenVehicle = this.form.get('choosenVehicle').value;
          this.setVehicle(choosenVehicle);
          this.disableEditVehicleInputs();
        }
      }
    );
  }

  fetchPathComboOptions() {

    this.findTrackService.fetchPathComboOptions().subscribe(
      (paths: MapPoint[]) => {
        this.paths = paths;
      }
    );

  }

  fetchPredefinedVehiclesComboOptions() {

    this.findTrackService.fetchPredefinedVehiclesComboOptions().subscribe(
      (predefinedVehicles: PredefinedVehicle[]) => {
        this.predefinedVehicles = predefinedVehicles;
      }
    );

  }

  buildForm() {

    this.form = this.formBuilder.group({
      from: ['', Validators.compose([])],
      fromQueryString: '',
      to: ['', Validators.compose([])],
      toQueryString: '',
      choosenVehicle: '',
      vehicleEdited: '',
      vehicle: this.formBuilder.group({
        height: [{value: null, disabled: true}, Validators.compose([])],
        width: [{value: null, disabled: true}, Validators.compose([])],
        mass: [{value: null, disabled: true}, Validators.compose([])],
        turningRadius: [{value: null, disabled: true}, Validators.compose([])]
      })
    });

  }

  disableEditVehicleInputs() {

    const vehicleControls = this.form.get('vehicle');

    vehicleControls.get('height').disable();
    vehicleControls.get('width').disable();
    vehicleControls.get('mass').disable();
    vehicleControls.get('turningRadius').disable();

  }

  enableEditVehicleInputs() {

    const vehicleControls = this.form.get('vehicle');

    vehicleControls.get('height').enable();
    vehicleControls.get('width').enable();
    vehicleControls.get('mass').enable();
    vehicleControls.get('turningRadius').enable();

  }

  setVehicle(vehicle: PredefinedVehicle) {

    if (vehicle) {

      this.form.get('vehicle').setValue({
        height: vehicle.height,
        width: vehicle.width,
        mass: vehicle.mass,
        turningRadius: vehicle.turningRadius
      });

    }

  }

  onSubmit() {

    alert('submituje se');

  }

  unselectFrom(): void {
    this.form.get('from').setValue(null);
  }

  unselectTo(): void {
    this.form.get('to').setValue(null);
  }

  unselectChosenVehicle(): void {
    this.form.get('choosenVehicle').setValue(null);
  }

}
