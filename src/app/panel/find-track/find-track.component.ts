import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-find-track',
  templateUrl: './find-track.component.html',
  styleUrls: ['./find-track.component.sass']
})
export class FindTrackComponent implements OnInit, OnDestroy {

  sidebarOpen = true;

  constructor() {
  }

  ngOnInit(): void {


  }

  ngOnDestroy(): void {


  }

}
