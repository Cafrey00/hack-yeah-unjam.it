import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {PaperScope, Project, Path} from 'paper';
import {getMapData, processRawData} from './map.service';
import {FindTrucksService} from '../../../_services/find-trucks.service';
import {NodeObj} from '../../../_models/node.model';
import {PathObj} from '../../../_models/path.model';


@Component({
  selector: 'app-find-truck-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})
export class MapComponent implements OnInit {
  @ViewChild('canvasElement') canvasElement: ElementRef;
  scope: PaperScope;
  project: Project;

  constructor(private findTrucksService: FindTrucksService) {

  }

  ngOnInit() {

    this.fetchMapData();

    this.scope = new PaperScope();
    this.project = new Project(this.canvasElement.nativeElement);

    const mapData = processRawData(getMapData(), this.project.view.size);

    mapData.paths.forEach((rawPath: { from: string, to: string }, i: number) => {
      const path = new Path();
      path.strokeColor = 'black';
      path.add(mapData.nodes[rawPath.from]);
      path.add(mapData.nodes[rawPath.to]);
      this.project.activeLayer.addChild(path);
    });
  }

  fetchMapData() {

    this.findTrucksService.fetchMapData().subscribe((response: {nodes: NodeObj[], paths: PathObj[]}) => {
      console.log( response );
    });

  }

}
