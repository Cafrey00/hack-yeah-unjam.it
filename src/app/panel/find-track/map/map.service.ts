import {Point} from 'paper';

interface IndexedNodes {
  [id: string]: Point;
}

interface MapData {
  nodes: IndexedNodes;
  paths: RawMapPath[];
}

interface RawMapNode {
  id: string;
  x: number;
  y: number;
}

interface RawMapPath {
  from: string;
  to: string;
}

interface RawMapData {
  nodes: RawMapNode[];
  paths: RawMapPath[];
}

interface MapDataBounds {
  minX: number;
  maxX: number;
  minY: number;
  maxY: number;
}

export const getMapData = (): RawMapData => {
  return {
    nodes: [
      {id: 'A', x: 0, y: 0},
      {id: 'B', x: 2, y: 0},
      {id: 'C', x: 2, y: 4},
      {id: 'D', x: 0, y: 4},
      {id: 'E', x: 0, y: 7},
      {id: 'F', x: 5, y: 7},
      {id: 'G', x: 7, y: 8},
      {id: 'H', x: 7, y: 0},
      {id: 'I', x: 2, y: -3},
      {id: 'J', x: 7, y: -3},
      {id: 'K', x: -2, y: 4},
      {id: 'L', x: -2, y: 7},
      {id: 'M', x: 5, y: 4},
      {id: 'N', x: 5, y: 8},
      {id: 'OUT', x: 0, y: 9},
    ],
    paths: [
      {from: 'A', to: 'B'},
      {from: 'A', to: 'D'},
      {from: 'B', to: 'H'},
      {from: 'B', to: 'I'},
      {from: 'C', to: 'B'},
      {from: 'C', to: 'D'},
      {from: 'C', to: 'M'},
      {from: 'M', to: 'F'},
      {from: 'J', to: 'H'},
      {from: 'J', to: 'I'},
      {from: 'H', to: 'G'},
      {from: 'G', to: 'N'},
      {from: 'N', to: 'F'},
      {from: 'F', to: 'E'},
      {from: 'E', to: 'L'},
      {from: 'E', to: 'D'},
      {from: 'L', to: 'K'},
      {from: 'K', to: 'D'},
      {from: 'E', to: 'OUT'},
    ]
  };
};

export const processRawData = (
  mapData: RawMapData,
  size: paper.Size,
): MapData => {
  const dataBounds = getMapDataBounds(mapData);
  const paginatedData = paginateMapData(mapData, dataBounds);
  const scaledData = scaleData(paginatedData, dataBounds, size);
  const nodesIndexed = indexNodes(scaledData.nodes);
  return {
    paths: scaledData.paths,
    nodes: nodesIndexed,
  };
};

const getMapDataBounds = (
  mapData: RawMapData,
) => {
  let minX: number = mapData.nodes[0].x;
  let maxX: number = mapData.nodes[0].x;
  let minY: number = mapData.nodes[0].y;
  let maxY: number = mapData.nodes[0].y;

  mapData.nodes.forEach((rawMapNode) => {
    if (rawMapNode.x < minX) {
      minX = rawMapNode.x;
    } else if (rawMapNode.x > maxX) {
      maxX = rawMapNode.x;
    }

    if (rawMapNode.y < minY) {
      minY = rawMapNode.y;
    } else if (rawMapNode.y > maxY) {
      maxY = rawMapNode.y;
    }
  });

  return {minX, maxX, minY, maxY};
};

const paginateMapData = (
  rawMapData: RawMapData,
  {minX, maxY}: MapDataBounds,
): RawMapData => {
  return {
    ...rawMapData,
    nodes: rawMapData.nodes.map((rawMapNode) => ({
      ...rawMapNode,
      x: rawMapNode.x - minX,
      y: Math.abs(rawMapNode.y - maxY),
    })),
  };
};

const scaleData = (
  rawMapData: RawMapData,
  {minX, maxX, minY, maxY}: MapDataBounds,
  size: paper.Size,
): RawMapData => {
  const deltaX = Math.abs(minX) + Math.abs(maxX);
  const deltaY = Math.abs(minY) + Math.abs(maxY);
  const ratioX = size.width / deltaX;
  const ratioY = size.height / deltaY;

  return {
    ...rawMapData,
    nodes: rawMapData.nodes.map((node) => ({
      ...node,
      x: node.x * ratioX,
      y: node.y * ratioY,
    })),
  };
};

const indexNodes = (nodes: RawMapNode[]): IndexedNodes => {
  return nodes.reduce((acc, {id, x, y}) => ({
    ...acc,
    [id]: new Point(x, y),
  }), {});
};
